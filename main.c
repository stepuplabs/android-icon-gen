#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <wand/magick_wand.h>

#define ICON_SIZE 1024
#define ICON_BG_SIZE (ICON_SIZE * 0.75)
#define ICON_FG_SIZE (ICON_SIZE * 0.55)
#define ICON_BG_PADDING ((ICON_SIZE - ICON_BG_SIZE) / 2)
#define ICON_FG_PADDING ((ICON_SIZE - ICON_FG_SIZE) / 2)

#define RICON_BG_SIZE (ICON_SIZE * 0.75)
#define RICON_FG_SIZE (((RICON_BG_SIZE-100)/2) * sqrt(2))
#define RICON_BG_PADDING ((ICON_SIZE - RICON_BG_SIZE) / 2)
#define RICON_FG_PADDING ((ICON_SIZE - RICON_FG_SIZE) / 2)

int aig_init(int argc, char* argv[]);
void aig_print_usage();
void aig_make_icon(const char *filename, const char *foreground_image, const char *background_image, int size, int rounded);
void aig_resize_image(const char *src, const char *dst, int width, int height);
void aig_make_foreground(const char *src, const char *dst, int size);
void aig_shadow_image(MagickWand *image, float opacity, float sigma, int x, int y);
int aig_copy_file(const char *src, const char *dst);
int aig_create_file(const char *filename, const char *content, size_t length);
void aig_term();

PixelWand *cNone  = NULL;
PixelWand *cWhite = NULL;
PixelWand *cBlack = NULL;

#define FILE_PATH_SIZE 2048

typedef struct options_st {
   char output[FILE_PATH_SIZE];
   char icon_background[FILE_PATH_SIZE];
   char icon_foreground[FILE_PATH_SIZE];
} options_t;

#define ARTIFACT_TYPE_DIRECTORY 0
#define ARTIFACT_TYPE_ICON 1

typedef struct icon_spec_st {
   char dir[FILE_PATH_SIZE];
   int icon_size;             // size of final item
   int component_size;        // size of background and foreground components
} icon_spec_t;

const char *xml_icon_dir = "mipmap-anydpi-v26";
const char *xml_icon = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                       "<adaptive-icon xmlns:android=\"http://schemas.android.com/apk/res/android\">\n"
                       "    <background android:drawable=\"@mipmap/ic_launcher_background\"/>\n"
                       "    <foreground android:drawable=\"@mipmap/ic_launcher_foreground\"/>\n"
                       "</adaptive-icon>\n";

const icon_spec_t icons[] = {
   { "mipmap-mdpi", 48, 108 },
   { "mipmap-hdpi", 72, 162 },
   { "mipmap-xhdpi", 96, 216 },
   { "mipmap-xxhdpi", 144, 324 },
   { "mipmap-xxxhdpi", 192, 432 },
   { "", 0, 0 }
};

options_t options;

int main(int argc, char *argv[]) {

   char path[FILE_PATH_SIZE];
   struct stat st;
   const icon_spec_t *pic = icons;

   if (aig_init(argc, argv) < 0) {
      return -1;
   }

   // creates output directory
   
   if (stat(options.output, &st) < 0) {
      mkdir(options.output, 0765);
   }

   // generate icons

   while (pic->icon_size > 0) {

      sprintf(path, "%s/%s", options.output, pic->dir);
      mkdir(path, 0765);

      sprintf(path, "%s/%s/ic_launcher.png", options.output, pic->dir);
      aig_make_icon(path, options.icon_foreground, options.icon_background, pic->icon_size, 0);

      sprintf(path, "%s/%s/ic_launcher_round.png", options.output, pic->dir);
      aig_make_icon(path, options.icon_foreground, options.icon_background, pic->icon_size, 1);

      sprintf(path, "%s/%s/ic_launcher_background.png", options.output, pic->dir);
      aig_resize_image(options.icon_background, path, pic->component_size, pic->component_size);
      
      sprintf(path, "%s/%s/ic_launcher_foreground.png", options.output, pic->dir);
      aig_make_foreground(options.icon_foreground, path, pic->component_size);

      pic++;
   }

   // create xml icons

   sprintf(path, "%s/%s", options.output, xml_icon_dir);
   mkdir(path, 0765);

   sprintf(path, "%s/%s/ic_launcher.xml", options.output, xml_icon_dir);
   aig_create_file(path, xml_icon, strlen(xml_icon));

   sprintf(path, "%s/%s/ic_launcher_round.xml", options.output, xml_icon_dir);
   aig_create_file(path, xml_icon, strlen(xml_icon));

   aig_term();

   return 0;
}

int aig_init(int argc, char *argv[]) {

   int optidx = 0;

   // read arguments

   memset(&options, 0, sizeof(options));

   while (optidx < argc) {

      int opt = getopt(argc, argv, "f:b:");

      if (opt != -1) {
         
         switch (opt) {
            case 'b': {
               strncpy(options.icon_background, optarg, sizeof(options.icon_background));
            } break;

            case 'f': {
               strncpy(options.icon_foreground, optarg, sizeof(options.icon_foreground));
            } break;
         }

      } else {

         if (argv[optidx] != NULL) {
            strncpy(options.output, argv[optidx], sizeof(options.output));
         }
      }

      optidx++;
   }

   // Validate arguments

   if (strlen(options.output) == 0 || strlen(options.icon_background) == 0 || strlen(options.icon_foreground) == 0) {
      aig_print_usage();
      return -1;
   }

   // Initialize MagickWand Library

	MagickWandGenesis();

   cNone  = NewPixelWand();
   cWhite = NewPixelWand();
   cBlack = NewPixelWand();

   PixelSetColor(cNone,  "none");
   PixelSetColor(cWhite, "white");
   PixelSetColor(cBlack, "black");

   return 0;
}

void aig_term() {

   DestroyPixelWand(cNone);
   DestroyPixelWand(cWhite);
   DestroyPixelWand(cBlack);

   MagickWandTerminus();
}

void aig_print_usage() {
   fprintf(stderr, "\n\n"
                   "Usage:\n"
                   "\tandroid-icon-gen -f <icon-foreground> -b <icon-background> <output directory>\n\n"
                   "\t- icon-foreground:\tpath to foreground image file.\n"
                   "\t- icon-background:\tpath to background image file or background color in hexadecimal format (#ffffff)\n\n");
}

void aig_make_icon(const char *filename, const char *foreground_image, const char *background_image, int size, int rounded) {

   const int icon_fg_size = (rounded) ? RICON_FG_SIZE : ICON_FG_SIZE;
   const int icon_fg_padding = (rounded) ? RICON_FG_PADDING : ICON_FG_PADDING;
   const int icon_bg_size = (rounded) ? RICON_BG_SIZE : ICON_BG_SIZE;
   const int icon_bg_padding = (rounded) ? RICON_BG_PADDING : ICON_BG_PADDING;

   // Alloc memory

   DrawingWand *draw = NewDrawingWand();
   MagickWand *icon = NewMagickWand();
   MagickWand *fg = NewMagickWand();
   MagickWand *bg = NewMagickWand();

   // Create Icon Image with transparent background
	MagickNewImage(icon, ICON_SIZE, ICON_SIZE, cNone);

   // Load icon foreground
   MagickReadImage(bg, background_image);
   //MagickResizeImage(bg, ICON_BG_SIZE, ICON_BG_SIZE, LanczosFilter, 1.0);

   // Load icon background
   MagickReadImage(fg, foreground_image);
   MagickResizeImage(fg, icon_fg_size, icon_fg_size, LanczosFilter, 1.0);

   // draw icon card

   DrawSetFillColor(draw, cWhite);
   
   if (rounded) {   
      DrawArc(draw, icon_bg_padding, icon_bg_padding, icon_bg_padding+icon_bg_size, icon_bg_padding+icon_bg_size, 0, 360);
   } else {
      DrawRoundRectangle(draw, icon_bg_padding, icon_bg_padding, icon_bg_padding+icon_bg_size, icon_bg_padding+icon_bg_size, 
                               ICON_SIZE*0.0625, ICON_SIZE * 0.0625);
   }

   MagickDrawImage(icon, draw);

   // apply icon background

   MagickCompositeImage(icon, bg, InCompositeOp, icon_bg_padding, icon_bg_padding);

   // apply icon foreground

   MagickCompositeImage(icon, fg, OverCompositeOp, icon_fg_padding, icon_fg_padding);

   // apply drop shadow

   aig_shadow_image(icon, 50, 0.8, 0, 0);

   // resize image

   if (size < ICON_SIZE) {
      MagickResizeImage(icon, size, size, LanczosFilter, 1);
   }

   // save image

   MagickWriteImage(icon, filename);

   // Cleanup memory

   DestroyMagickWand(icon);
   DestroyMagickWand(bg);
   DestroyMagickWand(fg);
   DestroyDrawingWand(draw);
}

void aig_shadow_image(MagickWand *image, float opacity, float sigma, int x, int y) {

   MagickWand *foreground = CloneMagickWand(image);
   MagickWand *black = CloneMagickWand(image);

   MagickNewImage(black, ICON_SIZE, ICON_SIZE, cBlack);
   MagickShadowImage(image, opacity, sigma, x, y);

   MagickCompositeImage(image, black, InCompositeOp, 0, 0);
   MagickCompositeImage(image, foreground, OverCompositeOp, 0, 0);

   DestroyMagickWand(foreground);
   DestroyMagickWand(black);
}

void aig_make_foreground(const char *src, const char *dst, int size) {

   double sw, sh, f, lp, tp;

   MagickWand *sImg = NewMagickWand();
   MagickWand *dImg = NewMagickWand();

   MagickReadImage(sImg, src);

   sw = (double)MagickGetImageWidth(sImg);
   f = RICON_FG_SIZE / sw;

   MagickTrimImage(sImg, 0);

   sw = (double)MagickGetImageWidth(sImg);
   sh = (double)MagickGetImageHeight(sImg);

   sw *= f;
   sh *= f;

   tp = (ICON_SIZE - sh) / 2;
   lp = (ICON_SIZE - sw) / 2;

   MagickResizeImage(sImg, (size_t)sw, (size_t)sh, LanczosFilter, 1.0);

   MagickNewImage(dImg, ICON_SIZE, ICON_SIZE, cNone);
   MagickCompositeImage(dImg, sImg, AddCompositeOp, lp, tp);
   MagickResizeImage(dImg, size, size, LanczosFilter, 1.0);

   MagickWriteImage(dImg, dst);

   DestroyMagickWand(sImg);
   DestroyMagickWand(dImg);
}

void aig_resize_image(const char *src, const char *dst, int width, int height) {

   MagickWand *img = NewMagickWand();

   MagickReadImage(img, src);
   MagickResizeImage(img, width, height, LanczosFilter, 1.0);
   MagickWriteImage(img, dst);

   DestroyMagickWand(img);
}

int aig_copy_file(const char *src, const char *dst) {

   char buf[4096];
   FILE *fSrc, *fDst;

   fSrc = fopen(src, "r");

   if (!fSrc) {
      return -1;
   }

   fDst = fopen(dst, "w");

   if (!fDst) {
      fclose(fSrc);
      return -1;
   }

   while (!feof(fSrc)) {

      int n;

      memset(buf, 0, sizeof(buf));

      n = fread(buf, 1, sizeof(buf), fSrc);

      if (n > 0) {
         fwrite(buf, 1, n, fDst);
      }
   }

   fclose(fSrc);
   fclose(fDst);

   return 0;
}

int aig_create_file(const char *filename, const char *content, size_t length) {

   FILE *f = fopen(filename, "w");

   if (!f) 
      return -1;

   fwrite(content, 1, length, f);

   fclose(f);

   return 0;
}
