# Android Icon Generator

It's a command line tool to generate android icons.

## Dependencies

```shell
sudo apt-get install -y libmagickwand-dev
```
