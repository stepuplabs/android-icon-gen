android-icon-gen: main.c
	gcc -o $@ $< `pkg-config --cflags --libs MagickWand`

clean:
	rm -f android-icon-gen

install:
	cp android-icon-gen /usr/bin/android-icon-gen
	chmod +x /usr/bin/android-icon-gen

test: android-icon-gen
	if [ -e icons ]; then rm -rf icons; fi
	./android-icon-gen -b appicon_background.png -f appicon.png icons

